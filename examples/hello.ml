let () =
  LLVM.print_context_module
    (LLVM.new_global
       ~link_type:(Some `Internal)
       ~constant:true
       "msg"
       (LLVM_types.Value (LLVM_types.string 13 "hello world!"))
    );
  LLVM.print_context_module
    (LLVM.new_declaration
       "puts"
       (LLVM_types.App
          (LLVM_types.Pointer (LLVM_types.Int LLVM_types.eight),
           LLVM_types.Ret (LLVM_types.Int LLVM_types.thirty_two)
          )
       )
      ~link_type:(Some `External)
    );
  LLVM.print_context_module
    (LLVM.new_definition
      "main"
      (LLVM_types.Ret (LLVM_types.Int LLVM_types.thirty_two))
      [LLVM.Ret (LLVM_types.int32 0)]
    )
